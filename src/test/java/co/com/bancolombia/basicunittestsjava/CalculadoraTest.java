package co.com.bancolombia.basicunittestsjava;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {

    @Before
    public void setUp () {

    }

    @After
    public void tearDown () {

    }

    @Test
    public void verificarSumarDosValores () throws Exception {
        // Arrange
        int num1_test = 10;
        int num2_test = 5;
        Calculadora calc = new Calculadora(num1_test, num2_test);

        // Act
        int resp = calc.sumarDosValores();

        // Assert
        assertEquals(15, resp);
    }

    @Test
    public void verificarNegativoSumaDosValores () throws Exception {
        int num1_test = -10;
        int num2_test = 5;
        Calculadora calc = new Calculadora(num1_test, num2_test);

        int resp = calc.sumarDosValores();

        assertEquals(0, resp);
    }

    @Test(expected = Exception.class)
    public void verificarExcepcionNegativosSumaDosValores () throws Exception {
        int num1_test = -10;
        int num2_test = -5;
        Calculadora calc = new Calculadora(num1_test, num2_test);

        calc.sumarDosValores();
    }

    @Test
    public void cuandoEsMayor20VerificarPositivo () {
        int num_test = 1000;
        Calculadora calc = new Calculadora();

        boolean resp = calc.verificarPositivo(num_test);

        assertTrue(resp);
    }

    @Test
    public void cuandoEsMenor20VerificarPositivo () {
        int num_test = 19;
        Calculadora calc = new Calculadora();

        boolean resp = calc.verificarPositivo(num_test);

        assertFalse(resp);
    }

}
