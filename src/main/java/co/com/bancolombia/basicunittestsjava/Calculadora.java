package co.com.bancolombia.basicunittestsjava;

public class Calculadora {

    private int num1;
    private int num2;

    public Calculadora () {

    }

    public Calculadora (int num1, int num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int sumarDosValores () throws Exception {

        if (this.num1 < 0 && this.num2 < 0) {
            throw new Exception("Todos son negativos!");
        }

        if (this.num1 < 0 || this.num2 < 0) {
            return 0;
        }
        return this.num1 + this.num2;
    }

    public boolean verificarPositivo (int num) {
        return num > 20;
    }
}
